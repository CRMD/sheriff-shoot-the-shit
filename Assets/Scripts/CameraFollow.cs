﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform mPlayer;
	public Vector3 mOffset;

	bool mShopping = false;
	Vector3 mOldPos;
	Vector3 mOldRot;
	public Transform mShopTransform;
	float startTime;
	public float mLerpSpeed;
	Vector3 mOffsetPos;
	bool mLerping;

	// Use this for initialization
	void Start () {
		if(!mPlayer)
		{
			mPlayer = GameObject.Find("Player").transform;
		}
		transform.position = new Vector3((mPlayer.position.x + mOffset.x), (mPlayer.position.y + mOffset.y), (mPlayer.position.z + mOffset.z));
		transform.LookAt(mPlayer);
	}
	
	// Update is called once per frame
	void Update () {
		if(!mLerping)
		{
			mOffsetPos = new Vector3((mPlayer.position.x + mOffset.x), (mPlayer.position.y + mOffset.y), (mPlayer.position.z + mOffset.z)) - transform.forward;
			if(!mShopping)
			{
				MouseLead();
				Time.timeScale = 1.0f;
				if(Input.GetKeyDown(KeyCode.M))
				{
					mOldPos = mOffsetPos;
					mOldRot = transform.rotation.eulerAngles;
					
					startTime = Time.time;
					mShopping = true;
					mLerping = true;
				}
			}
			else
			{
				Time.timeScale = 0.0f;
				if(Input.GetKeyDown(KeyCode.M))
				{
					startTime = Time.time;
					mShopping = false;
					mLerping = true;
				}
			}
		}
		else
		{
			Time.timeScale = 1.0f;
			ShopLerp ();
		}
	}

	bool CloseEnough(Vector3 vec1, Vector3 vec2)
	{
		float diff = 0.0001f;
		if(vec1.x < vec2.x + diff && vec1.x > vec2.x - diff
		   && vec1.y < vec2.y + diff && vec1.y > vec2.y - diff
		   && vec1.z < vec2.z + diff && vec1.z > vec2.z - diff)
		{
			return true;
		}
		return false;
	}

	void ShopLerp()
	{
		if(!mShopping)
		{
			transform.position = Vector3.Lerp(mShopTransform.position, mOldPos, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			transform.rotation = Quaternion.Slerp(mShopTransform.rotation, Quaternion.Euler(mOldRot), ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			
			if(CloseEnough(mOldPos, transform.position) && CloseEnough(mOldRot, transform.rotation.eulerAngles))
			{
				mLerping = false;
			}
		}
		else
		{
			transform.position = Vector3.Lerp(mOldPos, mShopTransform.position, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			transform.rotation = Quaternion.Slerp(Quaternion.Euler(mOldRot), mShopTransform.rotation, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			
			if(CloseEnough(transform.position, mShopTransform.position) && CloseEnough(transform.rotation.eulerAngles, mShopTransform.rotation.eulerAngles))
			{
				mLerping = false;
			}
		}
	}

	void MouseLead()
	{
		//transform.position = mOffsetPos;
		Vector3 MousePos = new Vector3((Input.mousePosition.x / Screen.currentResolution.width), 0f, (Input.mousePosition.y / Screen.currentResolution.height));
		MousePos = new Vector3(MousePos.x * transform.forward.x, 0f, MousePos.z * transform.forward.z);
		transform.position = Vector3.Lerp(transform.position, mOffsetPos + MousePos * 7, Time.deltaTime * 5f);
	}
}
