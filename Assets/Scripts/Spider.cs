﻿using UnityEngine;
using System.Collections;

public class Spider : MonoBehaviour {

	public SpiderState State = SpiderState.idle;

	private NavMeshAgent agent;
	public Transform player;
	public Animation anim;
	public float attackRange = 2.5f;

	// Use this for initialization
	void Start () {
		agent = gameObject.GetComponent<NavMeshAgent> ();
		anim = gameObject.GetComponentInChildren<Animation> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (agent.velocity.x < 0 || agent.velocity.x > 0 || agent.velocity.z < 0 || agent.velocity.z > 0)
		{
			State = SpiderState.walking;
		}
		else
		{
			State = SpiderState.idle;
		}
		if(Vector3.Distance(player.position, transform.position) <= attackRange)
		{
			State = SpiderState.attacking;
		}
		
		UpdateAnimations ();
	}

	void FixedUpdate()
	{
		agent.SetDestination (player.position);
	}

	void UpdateAnimations()
	{
		switch(State)
		{
		case SpiderState.idle:
			anim.Play("Idle");
			break;
		case SpiderState.attacking:
			anim.Play("Attack");
			break;
		case SpiderState.walking:
			anim.Play("Run");
			break;
		}
	}
}

public enum SpiderState
{
	idle,
	attacking,
	walking
}
