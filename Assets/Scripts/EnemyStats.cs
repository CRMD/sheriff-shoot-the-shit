﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour {
	
	public NavMeshAgent mAgent;

	public float mMaxHealth;
	private float mCurHealth;
	public float mDamage;
	private float mChargeTime;
	public float mSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
