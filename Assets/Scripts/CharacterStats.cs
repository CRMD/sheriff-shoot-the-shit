﻿using UnityEngine;
using System.Collections;

public class CharacterStats : MonoBehaviour {

	public float mPlayerMaxHealth;
	public float mPlayerMaxHealthDefault;
	public float mPlayerHealth;
	public int mPlayerMoney;
	public int mPlayerPotions;

	string mMoneyKey = "Sheriff Money";
	string mHealthKey = "Sheriff Health";
	string mPotionsKey = "Sheriff Potions";

	public bool debugWipe;

	void Awake()
	{
		if(debugWipe)
		{
			PlayerPrefs.DeleteKey(mMoneyKey);
			PlayerPrefs.DeleteKey(mHealthKey);
			PlayerPrefs.DeleteKey(mPotionsKey);
		}
	}

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey(mHealthKey))
		{
			mPlayerMaxHealth = PlayerPrefs.GetFloat(mHealthKey);
		}
		else
		{
			PlayerPrefs.SetFloat(mHealthKey, mPlayerMaxHealth);
		}
		if(PlayerPrefs.HasKey(mMoneyKey))
		{
			mPlayerMoney = PlayerPrefs.GetInt(mMoneyKey);
		}
		else
		{
			PlayerPrefs.SetInt(mMoneyKey, mPlayerMoney);
		}
		if(PlayerPrefs.HasKey(mPotionsKey))
		{
			mPlayerPotions = PlayerPrefs.GetInt(mPotionsKey);
		}
		else
		{
			PlayerPrefs.SetInt(mPotionsKey, mPlayerPotions);
		}

		mPlayerHealth = mPlayerMaxHealth;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDestroy()
	{
		PlayerPrefs.SetFloat(mHealthKey, mPlayerMaxHealth);
		PlayerPrefs.SetInt(mMoneyKey, mPlayerMoney);
		PlayerPrefs.SetInt(mPotionsKey, mPlayerPotions);
	}

	void OnGUI()
	{
		GUI.Label(new Rect(10.0f, 10.0f, 100.0f, 20.0f), "Health: " + mPlayerHealth.ToString() + "/" + mPlayerMaxHealth.ToString());
		GUI.Label(new Rect(10.0f, 30.0f, 100.0f, 20.0f), "Potions: " + mPlayerPotions.ToString());
		GUI.Label(new Rect(10.0f, 50.0f, 100.0f, 20.0f), "Money: " + mPlayerMoney.ToString());
	}
}
