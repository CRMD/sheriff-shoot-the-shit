﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	float mSpeed;
	float mDamage;

	// Use this for initialization
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0, 0, mSpeed * Time.deltaTime));//transform.forward * mSpeed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider col)
	{
		if (!col.gameObject.name.Contains("Bullet"))
		{
			col.gameObject.SendMessage("TakeDamage", mDamage, SendMessageOptions.DontRequireReceiver);
			Destroy (gameObject);
		}
	}
	void OnCollisionEnter(Collision col)
	{
		if (!col.gameObject.name.Contains("Bullet"))
		{
			col.gameObject.SendMessage("TakeDamage", mDamage, SendMessageOptions.DontRequireReceiver);
			Destroy (gameObject);
		}
	}

	public void SetValues (BulletProperties bp)
	{
		mSpeed = bp.mSpeed;
		mDamage = bp.mDamage;

		Destroy(gameObject, bp.mKillTime);
	}
}
