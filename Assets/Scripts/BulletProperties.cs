﻿using UnityEngine;
using System.Collections;

public class BulletProperties : MonoBehaviour {

	public float mSpeed;
	public float mDamage;
	public float mKillTime;

	public BulletProperties(float spd, float dmg, float kTime)
	{
		mSpeed = spd;
		mDamage = dmg;
		mKillTime = kTime;
	}
}
