﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {
	
	public int mMaxAmmo;						//The maximum amount of ammunition this weapon can hold
	private int mAmmo;							//The current ammunition that the weapon is holding
	public int mClipSize;						//The size of each clip
	private int mCurClip;						//The bullets remaining in the clip

	public bool mInfiniteAmmo;					//Toggle infinite ammo (still requires reloads
	public bool mBottomlessClip;				//Toggle bottomless clip

	public float mDamage;						//The total damage of each shot (will be divided by mShotsPerRound)
	public float mBulletSpeed;					//The speed the bullet travels

	private bool mReloading;					//If the gun is reloading
	public float mReloadTime;					//The amount of time it takes to reload the gun
	private float mCurReload;					//The amount of time left to finish reloading

	private Transform mPlayer;					//The Player Reference
	public GameObject mBullet;					//The GameObject that this gun uses for bullets

	public int mShotsPerRound;					//The number of bullets that are generated each shot (counts as 1 bullet)
	public float mSpread;						//The angle in front of the player that the rounds are spread with each shot

	public float mBulletKillTime;				//How long the bullet lasts before it destroys itself

	private BulletProperties mBulletProperties;	//Passed onto each bullet upon instantiation

	private Transform mPlayerHead;				//Direction player is looking, used for shooting

	void Start () {
		if(!mPlayerHead)
			mPlayerHead = GameObject.Find("Player/Ancient_Knight/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2").transform;
		if(!mPlayer)
			mPlayer = GameObject.Find("Player").transform;
		if(!mBullet)
			Debug.Log("Assign mBullet in inspector");
		mAmmo = mMaxAmmo;
		mCurClip = mClipSize;
		mBulletProperties = new BulletProperties(mBulletSpeed, (mDamage / mShotsPerRound), mBulletKillTime);
	}

	// Update is called once per frame
	void Update () {
		if(mReloading)
		{
			mCurReload -= Time.deltaTime;
			if(mCurReload <= 0)
			{
				mCurClip = mClipSize;
				if(!mInfiniteAmmo)
					mAmmo -= mClipSize;
				mReloading = false;
			}
		}
	}

	public void Reload()
	{
		if(!mReloading && mCurClip != mClipSize)
		{
			mReloading = true;
			mCurReload = mReloadTime;
		}
	}

	public void Fire()
	{
		if(mCurClip > 0 && !mReloading)
		{
			if(!mBottomlessClip)
				mCurClip--;
			if(mShotsPerRound > 1)
			{
				Debug.Log("Test1");
				for (int i = 0; i < mShotsPerRound; ++i)
				{
					Vector3 bulletRotation = BulletRotation(new Vector3(mPlayerHead.forward.x, 0.0f, mPlayerHead.forward.z),
					                                        -(mSpread/2) + ((mSpread/(float)mShotsPerRound) * i));
					Vector3 bulletPos = mPlayerHead.position + bulletRotation;
					                                  
					Vector3 bulletDir = bulletPos - mPlayerHead.position;
					GameObject bullet = (GameObject)Instantiate(mBullet,
					                                            bulletPos,
					                                            //Quaternion.FromToRotation(mPlayerHead.position, bulletPos));
					                                            Quaternion.LookRotation(bulletDir));
					float temp = mBulletProperties.mSpeed;
					mBulletProperties.mSpeed += Random.Range(-5.0f, 5.0f);
					bullet.SendMessage("SetValues", mBulletProperties, SendMessageOptions.DontRequireReceiver);
					mBulletProperties.mSpeed = temp;
					Debug.Log("Test2");
				}
			}
			else
			{
				Vector3 bulletPos = new Vector3(mPlayerHead.position.x + mPlayerHead.forward.x, mPlayerHead.position.y, mPlayerHead.position.z + mPlayerHead.forward.z);
				Vector3 bulletDir = bulletPos - mPlayerHead.position;
				Debug.Log (Quaternion.LookRotation(bulletDir));
				GameObject bullet = (GameObject)Instantiate(mBullet,
				                                            //new Vector3(mPlayerHead.forward.x, mPlayerHead.position.y, mPlayerHead.forward.z),
				                                            //Quaternion.Euler(0.0f, mPlayerHead.position.y, 0.0f));
				                                            bulletPos,
				                                            //Quaternion.AngleAxis(0.0f,new Vector3(0,1,0)));
															Quaternion.LookRotation(bulletDir));
				bullet.SendMessage("SetValues", mBulletProperties, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	
	private Vector3 BulletRotation(Vector3 vec3, float angle)
	{
		Vector3 retVec;

		retVec.x = vec3.x * Mathf.Cos(Mathf.Deg2Rad * angle) - vec3.z * Mathf.Sin(Mathf.Deg2Rad * angle);
		retVec.z = vec3.x * Mathf.Sin(Mathf.Deg2Rad * angle) + vec3.z * Mathf.Cos(Mathf.Deg2Rad * angle);
		retVec.y = vec3.y;

		return retVec;
	}

	void OnGUI()
	{
		if(mReloading)
			GUI.Label(new Rect(10.0f, 70.0f, 100.0f, 20.0f), "Reloading: " + mCurReload.ToString());
	}
}
