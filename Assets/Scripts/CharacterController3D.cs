﻿using UnityEngine;
using System.Collections;

public class CharacterController3D : MonoBehaviour {
//	public float spin=0.0f;
//	public float spin1=0.0f;
//	public float spin2=0.0f;
	public float mSpeed;
	Vector3 mWayPoint = Vector3.zero;
	Vector3 mMoveDir;

	public GameObject thing;
	
	public Animator anim;
	bool shooting = false;
	Vector3 oldPos;

	public GameObject mCurGun;
	private Transform mRightHand;
	// Use this for initialization
	void Start () {
		if(mRightHand == null)
		{
			mRightHand = transform.Find ("Ancient_Knight/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/mixamorig:RightHand");
			Debug.Log("'mRightHand' uses a temporary Transform, modify when final model is imported");

			Debug.Log("'bone1' uses a temporary Transform, modify when final model is imported");
			Debug.Log("'bone2' uses a temporary Transform, modify when final model is imported");
			Debug.Log("'bone3' uses a temporary Transform, modify when final model is imported");
		}
	}
	
	// Update is called once per frame
	void Update () {
		CheckInputs();
		if(mMoveDir != Vector3.zero)
			mMoveDir.Normalize();
		if(mCurGun.transform.parent == null)
		{
			mCurGun.transform.parent = mRightHand;
			mCurGun.transform.localPosition = Vector3.zero;
			mCurGun.transform.localRotation = Quaternion.Euler(Vector3.zero);
		}
		if(mWayPoint != Vector3.zero)
		{
			mMoveDir = mWayPoint - transform.position;
			mMoveDir.Normalize();
			if(transform.position.x < mWayPoint.x + 0.05f && transform.position.x > mWayPoint.x - 0.05f
			   && transform.position.z < mWayPoint.z + 0.05f && transform.position.z > mWayPoint.z - 0.05f)
			{
				mWayPoint = Vector3.zero;
			}
		}
		transform.position += (mMoveDir * mSpeed * Time.deltaTime);
		transform.LookAt(new Vector3(transform.position.x + mMoveDir.x, transform.position.y, transform.position.z + mMoveDir.z));
		UpdateAnimations ();
		mMoveDir = Vector3.zero;
	}
	void LateUpdate () 
	{
		oldPos = transform.position;
//		Transform bone1 = transform.Find ("Ancient_Knight/mixamorig:Hips/mixamorig:Spine");
////		bone1.rotation = Quaternion.AngleAxis (spin, new Vector3 (0, 0.25F, 0));
////		spin += Time.deltaTime * 15.0f;
//
//		Transform bone2 = transform.Find ("Ancient_Knight/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1");
////		bone2.rotation = Quaternion.AngleAxis (spin1, new Vector3 (0, 0.5F, 0));
////		spin1 += Time.deltaTime * 30.0f;
//
		Transform bone3 = transform.Find ("Ancient_Knight/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2");
////		bone3.rotation = Quaternion.AngleAxis (spin2, new Vector3 (0, 1, 0));
////		spin2 += Time.deltaTime * 60.0f;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			bone3.LookAt(new Vector3(hit.point.x, bone3.position.y, hit.point.z));
//			if(bone3.rotation.y > 180)
//			{
//				bone2.rotation = Quaternion.A(180 + (180 - bone3.rotation.eulerAngles.x/2.0f), 180 + (180 - bone3.rotation.eulerAngles.y/2.0f), 180 + (180 - bone3.rotation.eulerAngles.z/2.0f));
//				bone1.rotation.eulerAngles.Set(180 + (180 - bone2.rotation.eulerAngles.x/2.0f), 180 + (180 - bone2.rotation.eulerAngles.y/2.0f), 180 + (180 - bone2.rotation.eulerAngles.z/2.0f));
//			}
//			else
//			{
//				bone2.rotation.eulerAngles.Set(bone3.rotation.eulerAngles.x/2.0f, bone3.rotation.eulerAngles.y/2.0f, bone3.rotation.eulerAngles.z/2.0f);
//				bone1.rotation.eulerAngles.Set(bone2.rotation.eulerAngles.x/2.0f, bone2.rotation.eulerAngles.y/2.0f, bone2.rotation.eulerAngles.z/2.0f);
//			}
		}
	}
	void CheckInputs()
	{
		if(Input.GetKey(KeyCode.W))
		{
			mMoveDir += new Vector3(1, 0, 1);
			mWayPoint = Vector3.zero;
		}
		if(Input.GetKey(KeyCode.A))
		{
			mMoveDir += new Vector3(-1, 0, 1);
			mWayPoint = Vector3.zero;
		}
		if(Input.GetKey(KeyCode.S))
		{
			mMoveDir += new Vector3(-1, 0, -1);
			mWayPoint = Vector3.zero;
		}
		if(Input.GetKey(KeyCode.D))
		{
			mMoveDir += new Vector3(1, 0, -1);
			mWayPoint = Vector3.zero;
		}
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				if(hit.collider.gameObject.tag == "pathable")
				{
					GameObject temp = (GameObject)Instantiate(thing, hit.point, Quaternion.identity);
					Destroy (temp, 1.0f);
					mWayPoint = new Vector3(hit.point.x, hit.point.y, hit.point.z);
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.Mouse1))
		{
			mCurGun.SendMessage("Fire", SendMessageOptions.DontRequireReceiver);
		}
		if(Input.GetKey(KeyCode.Mouse0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				if(hit.collider.gameObject.tag == "pathable")
				{
					mWayPoint = new Vector3(hit.point.x, hit.point.y, hit.point.z);
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.Space))
		{
			mCurGun.SendMessage("Fire", SendMessageOptions.DontRequireReceiver);
		}
		if(Input.GetKeyDown(KeyCode.R))
		{
			mCurGun.SendMessage("Reload", SendMessageOptions.DontRequireReceiver);
		}
	}

	void UpdateAnimations ()
	{
		if(oldPos.x < transform.position.x - 0.5f * Time.deltaTime
		   || oldPos.x > transform.position.x + 0.5f * Time.deltaTime
		   || oldPos.z < transform.position.z - 0.5f * Time.deltaTime
		   || oldPos.z > transform.position.z + 0.5f * Time.deltaTime)
		{
//			anim.SetBool("attacking", true);
		}
		else
		{
//			anim.SetBool("attacking", false);
		}
//		if()
//		{
//			anim.SetBool("running", true);
//		}
//		else
//		{
//			anim.SetBool("running", false);
//		}
	}
}
